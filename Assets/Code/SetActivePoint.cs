﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetActivePoint : MonoBehaviour
{
    public GameObject Modal;

    // Start is called before the first frame update
    
    //void Start()
    //{
    //    Time.timeScale = 0;
    //    Modal.SetActive(true);
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Time.timeScale = 0;
        Modal.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
