﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[System.Serializable]
public class PlayerDataModel
{
    /// <summary>
    /// This is the level number of the next level to complete.
    /// </summary>
    public int level = 0;

    public int coins = 1;

    public int ski_level;
    public int jump_strength;
    public int jump_speed;
    public int rocket_level;
    public int energy_regen;
    public int torch_level;
    public List<string> coinsCollected = new List<string>();


    public PlayerDataModel() { }

    private static string path = Application.persistentDataPath + "/data.jam";


    public void SaveData()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, this);
        stream.Close();
    }

    public static PlayerDataModel Load()
    {
        if (File.Exists(path))
        {
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream stream = new FileStream(path, FileMode.Open))
                {
                    PlayerDataModel data = (PlayerDataModel)formatter.Deserialize(stream);
                    stream.Close();
                    return data;
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning("Error parsing data file. Deleting it. " + ex.Message);
                File.Delete(path);
                return new PlayerDataModel();
            }
        }
        else
        {
            Debug.Log("No Existing data found");
            return new PlayerDataModel();
        }
    }

    public static void DeleteData()
    {
        var data = new PlayerDataModel();
        data.level = 1;
        data.SaveData();
    }
}