﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour
{
    public GameObject LevelMenu;
    public SoundEffects SoundController;
    public int level;

    // Start is called before the first frame update
    void Start()
    {
        SetVisibility();
        ShopMenu.OnDataReset += ShopMenu_OnDataReset;
    }

    void ShopMenu_OnDataReset(object sender)
    {
        SetVisibility(); 
    }

    private void OnDestroy()
    {
        ShopMenu.OnDataReset -= ShopMenu_OnDataReset;
    }

    public void SetVisibility()
    {
        if (level > GameState.PlayerData.level && this.gameObject != null)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void Start_Level()
    {
        LevelMenu.SetActive(false);
        SoundController.StopLevelMusicWithNoise();
        GameState.CurrentLevel = level;
        SceneManager.LoadScene(level + 1);
        //StartCoroutine(startLevel());
    }

    IEnumerator startLevel()
    {
        var x1 = 0;
        var x2 = 0;
        //LevelMenu.SetActive(false);
        //SoundController.StopLevelMusicWithNoise();
        var y = Time.timeScale;
        yield return new WaitForSecondsRealtime(1f);

        var xY = 0;

        //GameState.CurrentLevel = level;
        //SceneManager.LoadScene(level + 1);
    }
    
}
