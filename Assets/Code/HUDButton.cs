﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HUDButton : MonoBehaviour
{
    public GameObject ConfirmModal;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void ShowModal()
    {
        Time.timeScale = 0;
        ConfirmModal.SetActive(true);
    }

    public void Confirm()
    {
        GameState.CurrentLevel = 0;
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }

    public void Dismiss()
    {
        ConfirmModal.SetActive(false);
        Time.timeScale = 1;
    }

    public void UnFocusButtons()
    {
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
    }
}
