﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject PreludeMenu;

    public void Start()
    {
        if(GameState.PlayerData.level == 0)
        {
            this.gameObject.SetActive(false);
            PreludeMenu.SetActive(true);
        }
    }   

    public void QuitGame()
    {
        Application.Quit();
    }
}
