﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
    static class MyLog
    {
        static int LogNumber = 0;

        public static void Log(string msg) {
            UnityEngine.Debug.Log("#" + LogNumber++ + ") " + msg);
        }
    }
}
