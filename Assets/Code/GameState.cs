﻿public static class GameState
{
    public static PlayerDataModel PlayerData
    {
        get
        {
            if (_playerData == null)
            {
                _playerData = PlayerDataModel.Load();
            }
            return _playerData;
        }
    }
    private static PlayerDataModel _playerData;


    public static void DeleteData ()
    {
        PlayerDataModel.DeleteData();
        _playerData = null;
    }

    public static int CurrentLevel;
}