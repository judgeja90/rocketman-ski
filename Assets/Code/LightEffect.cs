﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class LightEffect : MonoBehaviour
{

    public GameObject pointlight;
    private Light2D light2d;

    // Start is called before the first frame update
    void Start()
    {
        light2d = pointlight.GetComponent<Light2D>();
        //StartCoroutine(DestroyTimeout());
        
    }

    // Update is called once per frame
    void Update()
    {
        light2d.color = new Color()
        {
            r = light2d.color.r + (Random.value > .5 ? .1f : -.1f),
            g = light2d.color.g + (Random.value > .5 ? .1f : -.1f),
            b = light2d.color.b + (Random.value > .5 ? .1f : -.1f)
        };
        light2d.pointLightOuterRadius -= .05f;
        if(light2d.pointLightOuterRadius < .3f)
        {
            Destroy(this);
        }
    }

    //public IEnumerator DestroyTimeout()
    //{
    //    yield return new WaitForSecondsRealtime(1f);
    //    Destroy(this);
    //}

}
