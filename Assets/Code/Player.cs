﻿using Assets;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine.Experimental.Rendering.LWRP;
using TMPro;

public class Player : MonoBehaviour
{
    private const float max_fuel = 100f;
    //private const float start_energy = 1000f;
    private float fuel = 100f;
    //private float energy = 1000f;
    JumpStep jumpstep = JumpStep.None;
    private int collectedCoins = 0;
    private float jumpPower = 0f;
    private float m_BoostSpeed = 2f;
    private List<string> collectedCoinIds = new List<string>();

    private float m_MaxJumpForce = 200f;
    private float m_JumpChargeRate = 3f;
    //private float m_max_energy = 2000f;
    //private float m_energy_regen = 1f;
    private int m_ski_level = 0;
    private int m_torch_level = 1;
    private bool isDead = false;

    // Level coins
    private int m_levelCoins = 0;
    //private int m_alreadyCollectedLevelCoins = 0;


    private float starting_flashlightOuterRadius, starting_highlightOuterRadius;

    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement

    public Rigidbody2D rb;
    public Animator animator;
    public bool IsControlsEnabled = true;

    public GameObject start_modal;
    public GameObject end_modal;
    public GameObject alt_end_modal;
    public GameObject flashlight;
    public GameObject highlight;
    public GameObject lightEffect;

    public SoundEffects sound_effects;
    public Sprite[] skiSprites;

    [SerializeField] private LayerMask m_WhatIsGround;
    [SerializeField] private Transform m_GroundCheck;
    [SerializeField] private GameObject m_Skiis;
    [SerializeField] private TextMeshProUGUI m_txtCoinsCollected;
    //[SerializeField] private Slider m_EnergyBar;
    [SerializeField] private Slider m_FuelBar;

    private bool m_Grounded;            // Whether or not the player is grounded.
    private Rigidbody2D m_Rigidbody2D;

    private bool _level_already_completed
    {
        get
        {
            return GameState.CurrentLevel < GameState.PlayerData.level;
        }
    }

    [Header("Events")]
    [Space]
    public UnityEvent OnLandEvent;

    // can I delete this?
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    private void Awake()
    {
        _startpos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();
    }

    // Start is called before the first frame update
    void Start()
    {
        m_ski_level = GameState.PlayerData.ski_level;
        m_MaxJumpForce = (GameState.PlayerData.jump_strength * 100) + m_MaxJumpForce;
        m_JumpChargeRate = GameState.PlayerData.jump_speed + m_JumpChargeRate;
        m_BoostSpeed = GameState.PlayerData.rocket_level + m_BoostSpeed;
        //m_energy_regen = GameState.PlayerData.energy_regen + m_energy_regen;
        m_torch_level = GameState.PlayerData.torch_level;

        SetSkiis(m_ski_level);
        SetupLight(m_torch_level);
        StartCoroutine(initUIVariables());

        if (start_modal != null && !_level_already_completed)
        {
            start_modal.SetActive(true);
        }
    }

    private IEnumerator initUIVariables()
    {
        yield return new WaitForSecondsRealtime(.5f);
        m_levelCoins = GameObject.FindGameObjectsWithTag("Coin").Count();
        //m_alreadyCollectedLevelCoins = GameState.PlayerData.coinsCollected.Where(x => x.StartsWith(GameState.CurrentLevel + "_")).Count();
    }



    Vector3 _startpos;

    bool didboost = false;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            ResetLevel();
        }
        if (isDead || !IsControlsEnabled || Time.timeScale == 0) { return; }

        if (m_Grounded)
        {
            if (IsJumpInputDown())
            {
                jumpstep = JumpStep.Crouch;
                GetComponent<Rigidbody2D>().gravityScale = 2;
                animator.SetBool("IsCrouching", true);
            }
            else if (IsJumpUp())
            {
                animator.SetBool("IsCrouching", false);
                jumpstep = JumpStep.Jump;
                GetComponent<Rigidbody2D>().gravityScale = 1;
                sound_effects.PlayJumpSound();
            }
            else if (jumpstep == JumpStep.Boosting)
            {
                jumpstep = JumpStep.None;
                animator.SetBool("IsBoosting", false);
                animator.SetBool("IsCrouching", false);
            }
        }
        else
        {
            if (IsJumpInputDown() )
            {
                if(fuel > 0f)
                {
                    jumpstep = JumpStep.Boosting;
                    animator.SetBool("IsBoosting", true);
                    didboost = true;
                    sound_effects.PlayBoostSound();
                }
                else
                {
                    jumpstep = JumpStep.Crouch;
                    GetComponent<Rigidbody2D>().gravityScale = 2;
                    animator.SetBool("IsCrouching", true);
                }
            }
            else if (IsJumpUp() || (jumpstep == JumpStep.Boosting && fuel <= 0f))
            {
                jumpstep = JumpStep.None;
                animator.SetBool("IsBoosting", false);
                animator.SetBool("IsCrouching", false);
            }
        }
    }

    void FixedUpdate()
    {
        if (isDead || Time.timeScale == 0) { return; }

        bool wasGrounded = m_Grounded;
        m_Grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        const float k_GroundedRadius = .5f; // Radius of the overlap circle to determine if grounded

        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }


        Move();

        //if (energy < m_max_energy)
        //{
        //    energy += m_energy_regen;
        //}

        UpdateUI();
    }


    void Move()
    {
        if (m_Grounded)
        {

            if (jumpstep == JumpStep.Jump)
            {
                // Add a vertical force to the player.
                m_Grounded = false;
                m_Rigidbody2D.AddRelativeForce(new Vector2(20f, Mathf.Min(m_MaxJumpForce, jumpPower)));
                //StraightenRotation(m_Rigidbody2D);
                jumpstep = JumpStep.None;
                StraightenRotation(m_Rigidbody2D, 3f);
            }
            else if (jumpstep == JumpStep.Crouch)
            {
                //if (energy > 0)
                //{
                    jumpPower += m_JumpChargeRate;
                //    energy -= m_JumpChargeRate * 3f;
                //}
                //StraightenRotation(m_Rigidbody2D);
            }
        }
        else
        {
            if (jumpstep == JumpStep.Boosting)
            {
                // boosting should have a tendancy to upright the character.
                fuel -= 1f;
                m_Rigidbody2D.AddRelativeForce(new Vector2(m_BoostSpeed / 10, m_BoostSpeed));

                //StraightenRotation(m_Rigidbody2D, 3f);
                StraightenRotation(m_Rigidbody2D, 3f);
                SetLight(highlight, 3.5f);
            }
        }


        if (didboost && jumpstep != JumpStep.Boosting)
        {
            didboost = false;
            UnsetLight(highlight, starting_highlightOuterRadius);
            sound_effects.StopBoostSound();
        }
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (isDead) { return; }
        if (col.gameObject.tag == "Coin")
        {
            Coin coin = col.gameObject.GetComponent<Coin>();
            collectedCoins++;
            sound_effects.PlayCoinSound();
            Destroy(col.gameObject);
            Instantiate(lightEffect, this.transform, false);

            collectedCoinIds.Add(coin.Coin_Id);
        }
        else if (col.gameObject.name == "Dragon")
        {
            sound_effects.LevelEnd();
            GameState.PlayerData.coins += collectedCoins;
            GameState.PlayerData.coinsCollected.AddRange(collectedCoinIds);
            bool wasCompleted = _level_already_completed;
            if (!_level_already_completed) { GameState.PlayerData.level = GameState.CurrentLevel + 1; }
            GameState.PlayerData.SaveData();
            ShowEndModal(wasCompleted);
        }
        else if (col.gameObject.name == "DeathBox")
        {
            sound_effects.PlayDeathboxSound();
            PlayerDie();
        }
    }


    void OnCollisionEnter2D(Collision2D col)
    {
        if (isDead) { return; }
        if (col.gameObject.tag == "Ground" && col.otherCollider.name == "Player")
        {
            PlayerDie();
        }
    }

    /// <summary>
    /// Straightens to be slightly forwards
    /// </summary>
    private void StraightenRotation(Rigidbody2D body, float straightenAmount = 2f)
    {
        var upright = 170;
        var z = (body.transform.rotation.eulerAngles.z + 180) % 360;
        if (z != upright)
        {
            if (Math.Abs(z) < straightenAmount)
            {
                body.transform.rotation = Quaternion.Euler(0, 0, 0f);
            }
            else
            {
                body.transform.rotation = Quaternion.Euler(0, 0, (z > upright ? z - straightenAmount : z + straightenAmount) + 180);
                //tform.Rotate(0, 0, z > 0 ? -straightenAmount : straightenAmount);
            }
        }
        body.angularVelocity = 0;//+= body.angularVelocity > 0 ? -3 : 3;
    }

    public void OnLanding()
    {
        jumpPower = 0;
        //animator.SetBool("IsJumping", isJumping);
    }



    // set the material on bottom of player...
    private void SetSkiis(int ski_level)
    {
        double[] frictionLevels = { .2, .15, .1, .05, .03, .02, .01 };
        float friction = 0;
        int skiFrictionlevel = IsControlsEnabled ? ski_level : 0;
        if (skiFrictionlevel < frictionLevels.Length) { friction = Convert.ToSingle(frictionLevels[skiFrictionlevel]); }
        var skiObj = m_Skiis.GetComponent<BoxCollider2D>();
        skiObj.sharedMaterial.friction = friction;
        
        var skiSpriteRenderer = m_Skiis.GetComponent<SpriteRenderer>();
        skiSpriteRenderer.sprite = ski_level < skiSprites.Length ? skiSprites[ski_level] :  skiSprites.Last() ;
        //skiObj.friction = friction;
    }

    void UpdateUI()
    {
        if (IsControlsEnabled)
        {
            m_FuelBar.value = fuel / max_fuel;
            //m_EnergyBar.value = energy / m_max_energy;

            //int collectedCoins = collectedCoinIds.Count + m_alreadyCollectedLevelCoins;
            m_txtCoinsCollected.SetText(collectedCoinIds.Count + "/" + m_levelCoins);
        }
    }

    void ShowEndModal(bool wasAlreadyCompleted)
    {
        if (wasAlreadyCompleted)
        {
            if (alt_end_modal != null)
            {
                alt_end_modal.SetActive(true);
            }
        }
        else
        {
            if (end_modal != null)
            {
                end_modal.SetActive(true);
            }
        }
    }

    void PlayerDie()
    {
        isDead = true;
        MyLog.Log("Died");
        sound_effects.PlayDieSound();
        StartCoroutine(DieEffect());
        jumpstep = JumpStep.None;
        animator.SetBool("IsBoosting", false);
        animator.SetBool("IsCrouching", false);
    }

    IEnumerator DieEffect()
    {
        Time.timeScale = 0f;
        Light2D highlight2d = highlight.GetComponent<Light2D>();
        Light2D flashlight2d = flashlight.GetComponent<Light2D>();
        var start_flashIntensity = flashlight2d.intensity;
        //var start_highlightIntensity = highlight2d.intensity;
        var start_highlightColor = highlight2d.color;
        flashlight2d.intensity = 0;
        highlight2d.color = new Color(.7f, 0, 0);

        yield return new WaitForSecondsRealtime(1f);
        Time.timeScale = 2f;

        yield return new WaitForSecondsRealtime(1f);
        Time.timeScale = 1f;

        //highlight2d.intensity = start_highlightIntensity;
        highlight2d.color = start_highlightColor;
        flashlight2d.intensity = start_flashIntensity;
        ResetLevel();
    }

    public void ResetLevel()
    {
        transform.position = _startpos;
        m_Rigidbody2D.angularVelocity = 0;
        m_Rigidbody2D.velocity = new Vector2();
        transform.rotation = Quaternion.identity;
        fuel = max_fuel;
        //energy = start_energy;
        isDead = false;
    }


    public bool IsJumpInputDown()
    {
        if (Input.GetButtonDown("Jump")) { return true; }
        if (Input.GetMouseButtonDown(0)) { return true; }

        bool isTouching = false;
        if(Input.touchCount > 0)
        {
            isTouching = Input.GetTouch(0).phase == TouchPhase.Began;
        }
        return isTouching;

    }
    public bool IsJumpUp()
    {
        if (Input.GetButtonUp("Jump")) { return true; }
        if (Input.GetMouseButtonUp(0)) { return true; }
       
        bool isTouchUp = false;
        if(Input.touchCount > 0)
        {
            isTouchUp = Input.GetTouch(0).phase == TouchPhase.Ended;
        }
        return isTouchUp;
    }


    #region lighting

    private void SetupLight(float torch_level)
    {
        Light2D flashlight2d = flashlight.GetComponent<Light2D>();
        Light2D highlight2d = highlight.GetComponent<Light2D>();
        flashlight2d.pointLightOuterRadius += torch_level;
        highlight2d.pointLightOuterRadius += (torch_level * .5f);
        // store starting values
        starting_flashlightOuterRadius = flashlight2d.pointLightOuterRadius;
        starting_highlightOuterRadius = highlight2d.pointLightOuterRadius;
    }

    private void SetLight(GameObject light, float outerRadius)
    {
        Light2D light2d = light.GetComponent<Light2D>();
        light2d.pointLightOuterRadius = outerRadius;
    }

    private void UnsetLight(GameObject light, float startingOuterRadius)
    {
        Light2D light2d = light.GetComponent<Light2D>();
        light2d.pointLightOuterRadius = startingOuterRadius;
    }
    #endregion
}

public enum JumpStep
{
    None = 0,
    Crouch = 1,
    Jump = 2,
    Boosting = 3
}