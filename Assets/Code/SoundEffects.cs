﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundEffects : MonoBehaviour
{
    public AudioSource CoinCollideAudio;
    public AudioSource SkiNoiseAudio;
    public AudioSource BoostAudio;
    public AudioSource JumpAudio;
    public AudioSource DieAudio;
    public AudioSource DeathboxAudio;
    public AudioSource LevelMusic;
    public AudioSource TransitionNoise;

    void Start()
    {
        LevelMusic.Play();
    }

    void Update()
    {

    }

    public void PlayJumpSound()
    {
        JumpAudio.Play();
    }
     public void PlayDieSound()
    {
        DieAudio.Play();
    }

    public void PlayGroundSound()
    {
        if (!SkiNoiseAudio.isPlaying)
        {
            SkiNoiseAudio.Play();

        }
    }
    public void StopGroundSound()
    {
        SkiNoiseAudio.Stop();
    }

    public void PlayBoostSound()
    {
        if (!BoostAudio.isPlaying)
        {
            BoostAudio.Play();
        }
    }
    public void StopBoostSound()
    {
        BoostAudio.Stop();
    }

    public async Task PlayCoinSound()
    {
        CoinCollideAudio.Play();
    }

    public void StopLevelMusicWithNoise()
    {
        TransitionNoise.Play();
        LevelMusic.Stop();
    }


    public void PlayDeathboxSound()
    {
        DeathboxAudio.Play();
    }


    public enum Sounds
    {
        Coin,
        Ski,
        Boost
    }

    internal void LevelEnd()
    {
        LevelMusic.Stop();
        SkiNoiseAudio.Stop();
        BoostAudio.Stop();
    }
}
