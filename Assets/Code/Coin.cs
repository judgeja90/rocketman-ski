﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    //[HideInInspector]
    public string Coin_Id { get { return GameState.CurrentLevel + "_" + name; } }

    // Start is called before the first frame update
    void Start()
    {
        if (GameState.PlayerData.coinsCollected.Contains(Coin_Id))
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
