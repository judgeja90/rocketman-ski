﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogueCredits : MonoBehaviour
{
    public AudioSource soundClip;
    public TextMeshProUGUI text;
    public string[] Texts;

    private int textIdx = 0;

    // Start is called before the first frame update
    void Start()
    {
        text.SetText(Texts[textIdx]);
        
        if(soundClip != null)
        {
            soundClip.Play();
        }

        StartCoroutine(NextSlide());
    }

    private IEnumerator NextSlide()
    {
        yield return new WaitForSecondsRealtime(4);
        textIdx++;
        if (textIdx == Texts.Length)
        {
            Application.Quit();
        }
        else
        {
            text.SetText(Texts[textIdx]);
            StartCoroutine(NextSlide());
        }
    }

}
