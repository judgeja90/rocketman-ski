﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dialogue : MonoBehaviour
{
    public bool isEndModal;
    public GameObject nextModal;
    public TextMeshProUGUI nextText;

    public AudioSource soundClip;
    public bool isAutoDismiss;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        if(soundClip != null)
        {
            soundClip.Play();
            if (isAutoDismiss)
            {
                StartCoroutine(AutoDismiss());
            }
        }

        if (isAutoDismiss)
        {
            nextText.gameObject.SetActive(false);
        }
       
    }

    private IEnumerator AutoDismiss()
    {
        yield return new WaitForSecondsRealtime(soundClip.clip.length + .3f);
        Dismiss();
    }


    public void Dismiss()
    {
        if (nextModal != null)
        {
            nextModal.SetActive(true);
            this.gameObject.SetActive(false);
        }
        else if (isEndModal)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            this.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }


}
