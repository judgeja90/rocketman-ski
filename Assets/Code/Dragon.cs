﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : MonoBehaviour
{

    public Animator animator;
    public DragonType dragonType;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetInteger("DragonType", (int)dragonType);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public enum DragonType
    {
        Emo = 0,
        Slut = 1,
        Gimp = 2,
        Trump = 3
    }
}
