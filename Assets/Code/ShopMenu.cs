﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShopMenu : MonoBehaviour
{

    [SerializeField]
    private GameObject obj_coins;

    [SerializeField]
    private GameObject obj_ski_level;

    [SerializeField]
    private GameObject obj_jump_strength;

    [SerializeField]
    private GameObject obj_jump_speed;

    [SerializeField]
    private GameObject obj_rocket_level;

    //[SerializeField]
    //private GameObject obj_energy_regen;

    [SerializeField]
    private GameObject obj_torch_level;

    public delegate void DataResetEventHandler(object sender);
    public static event DataResetEventHandler OnDataReset;

    public void Start()
    {
        SetupTexts();
    }

    public void SetupTexts()
    {
        obj_coins.GetComponent<TextMeshProUGUI>().text = (GameState.PlayerData.coins).ToString();
        
        SetButtons(obj_ski_level, GameState.PlayerData.ski_level);

        SetButtons(obj_jump_strength, GameState.PlayerData.jump_strength);

        SetButtons(obj_jump_speed, GameState.PlayerData.jump_speed);

        SetButtons(obj_rocket_level, GameState.PlayerData.rocket_level);

        //SetButtons(obj_energy_regen, GameState.PlayerData.energy_regen);

        SetButtons(obj_torch_level, GameState.PlayerData.torch_level);
    }

    // Start is called before the first frame update
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void DeleteData()
    {
        GameState.DeleteData();
        SetupTexts();
        OnDataReset.Invoke(this);
    }


    public void SetButtons(GameObject button, int current_value)
    {
        int upgradeCost = current_value + 1;
        var textFields = button.GetComponentsInChildren<TextMeshProUGUI>();
        textFields.FirstOrDefault(tf => tf.name == "LevelText").text = (current_value + 1).ToString();
        textFields.FirstOrDefault(tf => tf.name == "CoinsText").text = "x" + (upgradeCost).ToString();
        //button.SetActive(GameState.PlayerData.coins >= upgradeCost);
        button.GetComponent<Button>().interactable = GameState.PlayerData.coins >= upgradeCost;
        //var texts = obj_coins.GetComponents<TextMeshProUGUI>().ToList();
        //texts.FirstOrDefault(t => t.tag == "");
    }
    #region ButtonHandlers

    public void Buy_ski_level()
    {
        GameState.PlayerData.coins -= GameState.PlayerData.ski_level + 1;
        GameState.PlayerData.ski_level++;
        GameState.PlayerData.SaveData();
        SetupTexts();
    }

    public void Buy_jump_strength()
    {
        GameState.PlayerData.coins -= GameState.PlayerData.jump_strength + 1;
        GameState.PlayerData.jump_strength++;
        GameState.PlayerData.SaveData();
        SetupTexts();
    }

    public void Buy_jump_speed()
    {
        GameState.PlayerData.coins -= GameState.PlayerData.jump_speed + 1;
        GameState.PlayerData.jump_speed++;
        GameState.PlayerData.SaveData();
        SetupTexts();
    }

    public void Buy_rocket_level()
    {
        GameState.PlayerData.coins -= GameState.PlayerData.rocket_level + 1;
        GameState.PlayerData.rocket_level++;
        GameState.PlayerData.SaveData();
        SetupTexts();
    }

    //public void Buy_energy_regen()
    //{
    //    GameState.PlayerData.coins -= GameState.PlayerData.energy_regen + 1;
    //    GameState.PlayerData.energy_regen++;
    //    GameState.PlayerData.SaveData();
    //    SetupTexts();
    //}

    public void Buy_torch_level()
    {
        GameState.PlayerData.coins -= GameState.PlayerData.torch_level + 1;
        GameState.PlayerData.torch_level++;
        GameState.PlayerData.SaveData();
        SetupTexts();
    }

    #endregion



}
